import unittest

from AC2 import app

class AC2Test(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()    
    
    def test_1(self):
        response = self.app.get('/')
        self.assertEqual("Se o senhor estiver vendo isso é porque funcionou, tenha um bom dia", response.get_data(as_text=True)
                         , "Deu erro !!!")
        
    
           
if __name__ == "__main__":
    unittest.main() 